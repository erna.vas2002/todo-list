const btnAddTask = document.querySelector('.add-btn');
const listTask = document.querySelector('.content-task__list');
const input = document.querySelector('.content-data__input');
const divQuntity = document.querySelector('.content-quntity');

btnAddTask.addEventListener('click', () => {
  if(input.value === '') return
  createDeleteElement(input.value)
  input.value = ''
})

function createDeleteElement(value) {
  let li = document.createElement('li');
  let btn = document.createElement('button');

  li.className = 'content-task__list-item';
  li.innerHTML = `<span>${value}</span>`;
  listTask.append(li);

  btn.className = 'delete-btn';
  btn.innerHTML = 'delete';
  li.append(btn);

  
  btn.addEventListener('click', () => {
    listTask.removeChild(li);
  })
  li.addEventListener('click', (e) => {
    li.classList.toggle('active')
  })
}